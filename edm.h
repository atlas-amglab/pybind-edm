#include <vector>

struct ElectronAuxStore{
    ElectronAuxStore(
        const std::vector<float>& pt,
        const std::vector<float>& eta,
        const std::vector<float>& phi,
        const std::vector<float>& m
    );
    int size();
    std::vector<float> pt;
    std::vector<float> eta;
    std::vector<float> phi;
    std::vector<float> m;
};

struct Electron{
    Electron(int index, const ElectronAuxStore& store) : idx(index), store(&store) {};  
    float pt(){
        return store->pt[idx];
    }
    Electron(const Electron&) = default;
    int idx;
    const ElectronAuxStore* store;
};

typedef std::vector<Electron> ElectronContainer;

