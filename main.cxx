#include <iostream>
#include <vector>
#include "edm.h"

int main(){
    std::cout << "hello world" << std::endl;

    ElectronAuxStore aux(
        {100,200,300,400},
        {0,1,2,3},
        {4,5,6,7},
        {100,200,300,400}
    );


    ElectronContainer ec;
    for(int i = 0; i<aux.size();++i){
        ec.push_back(Electron(i,aux));
    }

    for(auto e : ec){
        std::cout << "Electron: " << e.pt() << std::endl;
    }
    return 0;
}

