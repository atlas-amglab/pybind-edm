#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>
#include "edm.h"

namespace py = pybind11;

PYBIND11_MAKE_OPAQUE(ElectronContainer);

PYBIND11_MODULE(example, m) {
    py::class_<ElectronAuxStore>(m, "ElectronAuxStore")
        .def(py::init<
            const std::vector<float>&,
            const std::vector<float>&,
            const std::vector<float>&,
            const std::vector<float>&
            >(
        ))
        .def("size", &ElectronAuxStore::size)
        .def_readonly("pt",&ElectronAuxStore::pt);

    py::class_<Electron>(m, "Electron")
        .def(py::init<
            int,
            const ElectronAuxStore&
            >(
        ))
        .def("pt", &Electron::pt);

    py::bind_vector<ElectronContainer>(m, "ElectronContainer");

}
