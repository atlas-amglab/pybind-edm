#include "edm.h"

ElectronAuxStore::ElectronAuxStore(
        const std::vector<float>& pt,
        const std::vector<float>& eta,
        const std::vector<float>& phi,
        const std::vector<float>& m
)
:  pt(pt), eta(eta), phi(phi), m(m) {   
}

int ElectronAuxStore::size() {return pt.size();}